<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_perusahaan extends CI_Model {

	function upload($data = array())
	{
		$this->db->insert($data);
	}
	public function provinsi(){
		return $this->db->order_by('name','ASC')
		->get('provinces')->result();
	}
	function kabupaten($provID){
		$kabupaten="<option value='0'>--Pilih--</option>";
		$this->db->order_by('name','ASC');
		$kab=$this->db->get_where('regencies', array('province_id'=>$provID));

		foreach ($kab->result() as $data){
			$kabupaten.='<option value="'.$data->id.'">'.$data->name.'</option>';	
		}
		return $kabupaten;
	}
	function kecamatan($kabID){
		$kecamatan="<option value='0'>--Pilih--</option>";
		$this->db->order_by('name','ASC');
		$kec=$this->db->get_where('districts', array('regency_id'=>$kabID));

		foreach ($kec->result() as $data){
			$kecamatan.='<option value="'.$data->id.'">'.$data->name.'</option>';	
		}
		return $kecamatan;
	}
	function kelurahan($kecID){
		$kelurahan="<option value='0'>--Pilih--</option>";
		$this->db->order_by('name','ASC');
		$kel=$this->db->get_where('villages', array('district_id'=>$kecID));

		foreach ($kel->result() as $data){
			$kelurahan.='<option value="'.$data->id.'">'.$data->name.'</option>';	
		}
		return $kelurahan;
	}
}

/* End of file m_perusahaan.php */
/* Location: ./application/models/m_perusahaan.php */