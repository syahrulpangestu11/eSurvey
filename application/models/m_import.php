<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_import extends CI_Model {

    public function upload() {
        $object = array(
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            'no_telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'nama_pj' => $this->input->post('nama_pj'),
            'telp_pj' => $this->input->post('telp_pj'),
            'no_npwp' => $this->input->post('no_npwp'),
            'no_tdp' => $this->input->post('no_tdp'),
            'no_siup' => $this->input->post('no_siup'),
            'no_iuitdi' => $this->input->post('no_iuitdi'),
            'no_api' => $this->input->post('no_api'),
            'no_nik' => $this->input->post('no_nik'),
            'tgl_npwp' => $this->input->post('tgl_npwp'),
            'tgl_tdp' => $this->input->post('tgl_tdp'),
            'tgl_siup' => $this->input->post('tgl_siup'),
            'tgl_iuitdi' => $this->input->post('tgl_iuitdi'),
            'tgl_api' => $this->input->post('tgl_api'),
            'tgl_nik' => $this->input->post('tgl_nik'),
            'longitude' => $this->input->post('longitude'),
            'latitude' => $this->input->post('latitude'),
            'hadir' => $this->input->post('hadir'),
            'permasalahan' => $this->input->post('permasalahan'),
            'harapan' => $this->input->post('harapan'),
            'contoh_barang' => $this->input->post('contoh_barang')
        );
        $this->db->insert('import', $object);
        $insert_id = $this->db->insert_id();

        // Insert Detail Barang Komoditas
        $barang_komoditas = $this->input->post('list_barang_komoditas');
        $negara_asal = $this->input->post('list_negara_asal');
        $dokumen_perizinan = $this->input->post('list_dokumen_perizinan');
        for ($x = 0; $x < count($barang_komoditas); $x++) {
            $insert = array(
                'barang_komoditas' => $barang_komoditas[$x],
                'negara_asal' => $negara_asal[$x],
                'dokumen_perizinan' => $dokumen_perizinan[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('komoditas_impor', $insert);
        }
        
        // Insert Detail Penggunaan Barang
        $barang_penggunaan = $this->input->post('list_barang_penggunaan');
        $penggunaan = $this->input->post('list_penggunaan');
        for ($x = 0; $x < count($barang_komoditas); $x++) {
            $insert = array(
                'barang_penggunaan' => $barang_penggunaan[$x],
                'penggunaan' => $penggunaan[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('penggunaan_barang', $insert);
        }
        
        // Insert Detail Distribusi Barang
        $nama_pembeli = $this->input->post('list_nama_pembeli');
        $barang_distribusi = $this->input->post('list_barang_distribusi');
        for ($x = 0; $x < count($nama_pembeli); $x++) {
            $insert = array(
                'nama_pembeli' => $nama_pembeli[$x],
                'barang_distribusi' => $barang_distribusi[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('distribusi_barang', $insert);
        }
        
        // Insert Detail Bahan Produksi
        $jenis_bahan = $this->input->post('list_jenis_bahan');
        $kebutuhan = $this->input->post('list_kebutuhan');
        $asal_bahan = $this->input->post('list_asal_bahan');
        for ($x = 0; $x < count($jenis_bahan); $x++) {
            $insert = array(
                'jenis_bahan' => $nama_pembeli[$x],
                'kebutuhan' => $kebutuhan[$x],
                'asal_bahan' => $asal_bahan[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('bahan_produksi', $insert);
        }
        
        // Insert Detail Hasil Produksi
        $barang_produksi = $this->input->post('list_barang_produksi');
        $kapasitas = $this->input->post('list_kapasitas');
        for ($x = 0; $x < count($jenis_bahan); $x++) {
            $insert = array(
                'barang_produksi' => $barang_produksi[$x],
                'kapasitas' => $kapasitas[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('hasil_produksi', $insert);
        }
        
        // Insert Detail Pemasaran
        $barang_pemasaran = $this->input->post('list_barang_pemasaran');
        $p_lokal = $this->input->post('list_p_lokal');
        $p_ekspor = $this->input->post('list_p_ekspor');
        for ($x = 0; $x < count($barang_pemasaran); $x++) {
            $insert = array(
                'barang_pemasaran' => $barang_pemasaran[$x],
                'p_lokal' => $p_lokal[$x],
                'p_ekspor' => $p_ekspor[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('pemasaran', $insert);
        }
        
        // Insert Detail Realisasi Impor
        $komoditi = $this->input->post('list_komoditi');
        $asal_impor = $this->input->post('list_asal_impor');
        $volume = $this->input->post('list_volume');
        $nilai = $this->input->post('list_nilai');
        for ($x = 0; $x < count($komoditi); $x++) {
            $insert = array(
                'komoditi' => $komoditi[$x],
                'asal_impor' => $asal_impor[$x],
                'volume' => $volume[$x],
                'nilai' => $nilai[$x],
                'id_perusahaan' => $insert_id
            );
            $this->db->insert('realisasi_impor', $insert);
        }
        
        return $insert_id;
    }

    // public function get_import(){
    // 	$this->db->order_by('id_perusahaan', 'DESC');
    // 	$this->db->limit(1);
    // 	// $this->db->row();
    // 	return $this->db->get('import')->result();
    // }
}

/* End of file m_import.php */
/* Location: ./application/models/m_import.php */