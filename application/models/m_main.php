<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_main extends CI_Model {
	public function show_export()
	{
		return $this->db->get('export')->result();
	}
	public function show_import()
	{
		return $this->db->get('import')->result();
	}
}

/* End of file m_main.php */
/* Location: ./application/models/m_main.php */