<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_select extends CI_Model {
	public function provinsi()
	{
		return $this->db->order_by('name','ASC')
		->get('provinces')->result();
	}
	function kabupaten($provID){
		$kabupaten="<option value='0'>--pilih--</option>";
		$this->db->order_by('name','ASC');
		$kab=$this->db->get_where('regencies', array('province_id'=>$provID));

		foreach ($kab->result() as $data){
			$kabupaten.='<option value="'.$data->id.'">'.$data->name.'</option>';	
		}
		return $kabupaten;
	}

}

/* End of file m_select.php */
/* Location: ./application/models/m_select.php */