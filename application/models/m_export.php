<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_export extends CI_Model {
	/*public function upload()
	{
		$data = array(
			'id_perusahaan' => $this->input->post('id_perusahaan'),
			'sbh' => $this->input->post('sbh'),
			'itdp' => $this->input->post('itdp'),
			'tgl_itdp' => $this->input->post('tgl_itdp'),
			'npwp' => $this->input->post('npwp'),
			'siup' => $this->input->post('siup'),
			'tgl_siup' => $this->input->post('tgl_siup'),
			'jenis_eksportir' => $this->input->post('jenis_eksportir'),
			'iui' => $this->input->post('iui'),
			'tgl_iui' => $this->input->post('tgl_iui'),
			'bidang_usaha' => $this->input->post('bidang_usaha'),
			'uraian' => $this->input->post('uraian'),
			'asal_bahan' => $this->input->post('asal_bahan'),
			'ngr_ekspor' => $this->input->post('ngr_ekspor'),
			'kap_produksi' => $this->input->post('kap_produksi'),
			'kr_asal_barang' => $this->input->post('kr_asal_barang'),
			'hadir' => $this->input->post('hadir'),
			'temuan' => $this->input->post('temuan'),
			'arahan' => $this->input->post('arahan'),
			'saran' => $this->input->post('saran'),
			'sample' => $this->input->post('sample') 
		);
		return $this->db->insert('export',$data);
	}*/
	function insert($table = null, $data = array())
	{
		/*$jumlah = count($data);

		if($jumlah > 0){
			$this->db->insert_batch($table, $data);
		}*/
		$this->db->insert($data);
	}
	public function getpt()
	{
		return $this->db->join('perusahaan','perusahaan.id=export.id')
						->get('export')->result();
	}
}

/* End of file m_export.php */
/* Location: ./application/models/m_export.php */