<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

    function add_multiple() {
        if ($_POST == NULL) {
            $this->load->view('add_multiple');
        } else {
            redirect('data_mahasiswa/add_multiple_post/' . $_POST['banyak_data']);
        }
    }

    function add_multiple_post($banyak_data = 0) {
        if ($_POST == NULL) {
            $data['banyak_data'] = $banyak_data;
            $this->load->view('add_multiple_form', $data);
        } else {
            foreach ($_POST['data'] as $d) {
                $this->db->insert('mahasiswa', $d);
            }
            redirect('data_mahasiswa/lihat_data');
        }
    }
}

    /* End of file Mahasiswa.php */
    /* Location: ./application/controllers/Mahasiswa.php */    