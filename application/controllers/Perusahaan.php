<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url'));
		$this->load->model('m_perusahaan');
	}
	public function index()
	{
		$data['provinsi']=$this->m_perusahaan->provinsi();
		$data['content']='perusahaan';
		$this->load->view('home',$data);
	}
	public function add()
	{
		$data = array(
			'nama_perusahaan' => $this->input->post('nama_perusahaan'),
			'waktu' => $this->input->post('waktu'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
			'jalan' => $this->input->post('jalan'),
			'desa' => $this->input->post('desa'),
			'kecamatan' => $this->input->post('kecamatan'),
			'kabupaten' => $this->input->post('kabupaten'),
			'provinsi' => $this->input->post('provinsi'),
			'jalan_gudang' => $this->input->post('jalan_gudang'),
			'desa_gudang' => $this->input->post('desa_gudang'),
			'kecamatan_gudang' => $this->input->post('kecamatan_gudang'),
			'kabupaten_gudang' => $this->input->post('kabupaten_gudang'),
			'provinsi_gudang' => $this->input->post('provinsi_gudang'),
			'pj_perusahaan' => $this->input->post('pj_perusahaan'),
			'e_mail_pj' => $this->input->post('e_mail_pj'),
			'no_telp' => $this->input->post('no_telp')
		);
		$this->session->set_userdata($data);
		$this->db->insert('perusahaan',$data);
		redirect('Export/index');
	}
	public function ambil_data()
	{
		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kabupaten"){
			echo $this->m_perusahaan->kabupaten($id);
		} 
		else if($modul=="kecamatan") {
			echo $this->m_perusahaan->kecamatan($id);
		} 
		else if($modul=="kelurahan") {
			echo $this->m_perusahaan->kelurahan($id);
		}
	}
}

/* End of file Perusahaan.php */
/* Location: ./application/controllers/Perusahaan.php */