...<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_export');
		$this->load->helper('url','form');
	}
	public function index()
	{
		$data['getpt']=$this->m_export->getpt();
		$data['content']='export';
		$this->load->view('home', $data);
	}
	public function add()
	{
		$post = $this->input->post();
		
			foreach ($post['uraian'] as $key => $value){
				if($post['uraian'][$key] != ''){
					$simpan[] = array(
						'id' => $this->session->userdata('id'),
						'uraian' => $post['uraian'][$key],
						'sbh' => $this->input->post('sbh'),
						'itdp' => $this->input->post('itdp'),
						'tgl_itdp' => $this->input->post('tgl_itdp'),
						'npwp' => $this->input->post('npwp'),
						'siup' => $this->input->post('siup'),
						'tgl_siup' => $this->input->post('tgl_siup'),
						'jenis_eksportir' => $this->input->post('jenis_eksportir'),
						'iui' => $this->input->post('iui'),
						'tgl_iui' => $this->input->post('tgl_iui'),
						'bidang_usaha' => $this->input->post('bidang_usaha'),
						'asal_bahan' => $this->input->post('asal_bahan'),
						'ngr_ekspor' => $this->input->post('ngr_ekspor'),
						'kap_produksi' => $this->input->post('kap_produksi'),
						'kr_asal_barang' => $this->input->post('kr_asal_barang'),
						'hadir' => $this->input->post('hadir'),
						'temuan' => $this->input->post('temuan'),
						'arahan' => $this->input->post('arahan'),
						'saran' => $this->input->post('saran'),
						'sample' => $this->input->post('sample')
					);
				}
			} 
		$this->m_export->insert('export', $simpan);
		redirect('Main/index');
		
	}
	public function send()
	{
		$data = array(
			'id' => $this->session->userdata('id'),
			'uraian' => $this->input->post('uraian'),
			'sbh' => $this->input->post('sbh'),
			'itdp' => $this->input->post('itdp'),
			'tgl_itdp' => $this->input->post('tgl_itdp'),
			'npwp' => $this->input->post('npwp'),
			'siup' => $this->input->post('siup'),
			'tgl_siup' => $this->input->post('tgl_siup'),
			'jenis_eksportir' => $this->input->post('jenis_eksportir'),
			'iui' => $this->input->post('iui'),
			'tgl_iui' => $this->input->post('tgl_iui'),
			'bidang_usaha' => $this->input->post('bidang_usaha'),
			'asal_bahan' => $this->input->post('asal_bahan'),
			'ngr_ekspor' => $this->input->post('ngr_ekspor'),
			'kap_produksi' => $this->input->post('kap_produksi'),
			'kr_asal_barang' => $this->input->post('kr_asal_barang'),
			'hadir' => $this->input->post('hadir'),
			'temuan' => $this->input->post('temuan'),
			'arahan' => $this->input->post('arahan'),
			'saran' => $this->input->post('saran'),
			'sample' => $this->input->post('sample')
		);
		$this->db->insert('export', $data);
		redirect('Main/index','refresh');
	}
}
/* End of file Export.php */
/* Location: ./application/controllers/Export.php */
?>