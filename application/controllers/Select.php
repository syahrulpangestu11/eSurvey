<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Select extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_select');
	}
	public function index()
	{
		$data['provinsi']=$this->m_select->provinsi();
		$data['content']='select';
		$this->load->view('home', $data);
	}
	function ambil_data(){
		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kabupaten"){
			echo $this->m_select->kabupaten($id);
		}
		else if($modul=="kecamatan"){
			echo $this->m_select->kecamatan($id);
		}
	}
}

/* End of file Select.php */
/* Location: ./application/controllers/Select.php */