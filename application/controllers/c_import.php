<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_import extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_import');
    }

    public function upload() {
        if ($this->input->post('kirim')) {
            $this->load->model('m_import');
            if ($this->m_import->upload()) {
                redirect('main/index', 'refresh');
            } else {
                redirect('c_import/index', 'refresh');
            }
        } else {
            echo "fail";
        }
    }

    public function save() {
        $import_list = $this->input->post("#tbody");

        $this->load->model('m_import');
        $status = $this->m_import->save($import_list);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status' => $status));
    }

    public function save1() {
        $import_list1 = $this->input->post("#tbody1");

        $this->load->model('m_import');
        $status1 = $this->m_import->save1($import_list1);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status1' => $status1));
    }

    public function save2() {
        $import_list2 = $this->input->post("#tbody2");

        $this->load->model('m_import');
        $status2 = $this->m_import->save2($import_list2);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status2' => $status2));
    }

    public function save3() {
        $import_list3 = $this->input->post("#tbody3");

        $this->load->model('m_import');
        $status3 = $this->m_import->save3($import_list3);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status3' => $status3));
    }

    public function save4() {
        $import_list4 = $this->input->post("#tbody4");

        $this->load->model('m_import');
        $status4 = $this->m_import->save4($import_list4);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status4' => $status4));
    }

    public function save5() {
        $import_list5 = $this->input->post("#tbody5");

        $this->load->model('m_import');
        $status5 = $this->m_import->save5($import_list5);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status5' => $status5));
    }

    public function save6() {
        $import_list6 = $this->input->post("#tbody6");

        $this->load->model('m_import');
        $status6 = $this->m_import->save6($import_list6);

        $this->output->set_content_type('application/json');
        echo json_encode(array('status6' => $status6));
    }

}

/* End of file c_import.php */
/* Location: ./application/controllers/c_import.php */