<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_main','main');
		$this->load->model('m_import');
	}
	public function index()
	{
		$data['e']=$this->main->show_export();
		$data['i']=$this->main->show_import();
		$data['content']='homepage';
		$this->load->view('home',$data);
	}
	public function export()
	{
		$data['content']='export';
		$this->load->view('home', $data);
	}
	public function import()
	{
		// $data['import'] = $this->m_import->get_import();
		$data['content']='import';
		$this->load->view('home', $data);
	}
	public function perusahaan()
	{
		$data['content']='perusahaan';
		$this->load->view('home', $data);
	}
	public function map()
	{
		$this->load->view('geolocation');
	}
	public function select()
	{
		$this->load->view('select');
	}
	public function date()
	{
		$this->load->view('datetimepicker');
	}
}
