<!-- <script type="text/javascript" src="<?=base_url();?>adminlte/dist/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?=base_url();?>adminlte/dist/js/jquery.mask.min.js"></script>
<script src="wget https://github.com/igorescobar/jQuery-Mask-Plugin/blob/master/jquery.mask.min.js "></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<?php 
$perusahaan = $this->db->get('perusahaan')->result();
?>
  <div class="content-wrapper">
    <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2" style="margin-left:5%!important;margin-right: 5%;">
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#">Step 1</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#">Step 2</a>
                  </li>
                  <li class="active"><a class="nav-link active" data-toggle="pill" href="#tab_1" > Step 3 </a></li>
                  <li><a class="nav-link" data-toggle="pill" href="#tab_2" > Step 4 </a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <form action="<?=base_url('Export/send')?>" method="POST" enctype="multipart/form-data">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Perusahaan :</label>
                      <select class="form-control" id="get-perusahaan">
                        <option value="">-- Pilih Perusahaan --</option>
                        <?php 
                          foreach ($perusahaan as $key) {
                            echo '<option value="'.$key->id.'">'.$key->nama_perusahaan.'</option>';                  
                          }
                        ?>
                      </select> 
                      <h5><?=$_SESSION['nama_perusahaan']?></h5>
                    </div>
                    <input type="hidden" name="id">
                    <label for="exampleInputEmail1">Status badan hukum</label>
                    <form>
                    <div class="form-group">
                      <label class="radio">
                        <input type="radio" name="sbh" id="sbh" value="Swasta nasional" checked>Swasta nasional
                      </label>
                      <label class="radio">
                        <input type="radio" id="sbh" name="sbh" value="PMDN">PMDN
                      </label>
                      <label class="radio">
                        <input type="radio" id="sbh" name="sbh" value="PMA">PMA
                      </label>
                    </div>
                    </form>
                    <label for="exampleInputEmail1">No dan Tanggal Tanda Daftar Perusahaan</label>
                    <table id="example" class="display" style="width:100%">
                      <thead>
                        <tr>
                          <th><h6>Dokumen</h6></th>
                          <th><h6 style="text-align: center">Nomor & Tanggal</h6></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Tanda Daftar Perusahaan</td>
                          <td><input type="number" class="form-control itdp" id="itdp" name="itdp" placeholder="NO ITDP"/>
                          <input type="date" class="form-control" id="tgl_itdp" name="tgl_itdp" placeholder="TGL ITDP"></td>
                        </tr>
                        <tr>
                          <br>
                        </tr>
                        <tr>
                          <td>Surat Ijin Usaha Perusahaan</td>
                          <td><input type="number" class="form-control" id="siup" name="siup" placeholder="NO SIUP"/>
                          <input type="date" class="form-control" id="tgl_siup" name="tgl_siup"></td>
                        </tr>
                        <tr>
                          <td>(IUI)Ijin Usaha Industri/TDI(Tanda Daftar Industri)</td>
                          <td><input type="number" class="form-control" id="iui" name="iui" placeholder="NO IUI">
                          <input type="date" class="form-control" id="tgl_iui" name="tgl_iui"></td>
                        </tr>
                        <tr>
                          <td>NPWP</td>
                          <td><input type="text" class="form-control npwp" class="npwp" name="npwp" placeholder="NPWP"></td>
                        </tr>
                      </tbody>
                    </table>
                  
                    <br>
                    <label for="exampleInputEmail1">Jenis Eksportir</label>
                    <div class="form-group">
                      <div class="form-group">
                      <label class="radio">
                        <input type="radio" name="jenis_eksportir" id="jenis_eksportir" value="Produsen" checked>Produsen
                      </label>
                      <label class="radio">
                        <input type="radio" id="jenis_eksportir" name="jenis_eksportir" value="Trading">Trading
                      </label>  
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <label for="exampleInputEmail1">Bidang Usaha/Klasifikasi Usaha Industri(Untuk Produsen)</label>
                      <div class="form-group">
                      <input type="text" class="form-control" name="bidang_usaha" id="bidang_usaha" placeholder="Klasifikasi Bidang Usaha">  
                      </div>
                    <label for="exampleInputEmail1">Uraian Barang</label>
                    <table id="myTable" class="table order-list">
                    <tHead>
                      <tr>
                        <td>Uraian Barang</td>
                      </tr>
                    </tHead>
                    <tbody>
                      <tr>
                        <!-- <td>1</td> -->
                        <td class="col-sm-6">
                          <input type="text" class="form-control" name="uraian" placeholder="Uraian barang">
                        </td>
                        <td class="col-sm-6">
                          <a class="deleteRow"></a>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                    <br>
                      <tr>
                        <td colspan="5">
                          <input type="button" class="btn btn-round" id="adduraian" value="+ Tambah" />
                        </td>
                      </tr>
                    </tfoot> 
                    </table>
                    <label for="exampleInputEmail1">Asal Bahan</label>
                    <table id="myTable" class="table add-column">
                    <tbody>
                        <tr>
                          <td>Asal Bahan</td>
                        </tr>
                        <tr>
                        <td class="col-sm-6">
                          <input type="text" class="form-control" name="asal_bahan" placeholder="Asal bahan baku">
                        </td>
                        <td class="col-sm-6">
                          <a class="deleteRow"></a>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <br>
                      <tr>
                        <td colspan="5">
                          <input type="button" class="btn btn-round" id="addasal" value="+ Tambah" />
                        </td>
                      </tr>
                    </tfoot>  
                    </div>
                    </table>
                    <label for="exampleInputEmail1">Negara Tujuan Ekspor</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="ngr_ekspor" id="ngr_ekspor" placeholder="Negara tujuan ekspor">  
                    </div>
                    <label for="exampleInputEmail1">Kapasitas Produksi</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="kap_produksi" id="kap_produksi" placeholder="Kapasitas produksi">  
                    </div>
                    <label for="exampleInputEmail1">Kriteria Asal Barang</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="kr_asal_barang" id="kr_asal_barang" placeholder="Kriteria asal barang">  
                    </div>
                    <br>
                    <h5>Informasi Tambahan</h5>
                    <label for="exampleInputEmail1">Hadir Dalam Pertemuan</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="hadir" id="hadir" placeholder="Hadir dalam pertemuan">  
                    </div>
                    <label for="exampleInputEmail1">Temuan</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="temuan" id="temuan" placeholder="Temuan">  
                    </div>
                    <label for="exampleInputEmail1">Arahan</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="arahan" id="arahan" placeholder="Arahan">  
                    </div>
                    <label for="exampleInputEmail1">Yang Perlu Difasilitasi</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="saran" id="saran" placeholder="Yang perlu difasilitasi">  
                    </div>
                    <label for="exampleInputEmail1">Sample Barang</label>
                    <div class="form-group">
                      <input type="text" class="form-control" name="sample" id="sample" placeholder="Sample barang">  
                    </div>
                    <button type="submit" class="btn btn-success" name="submit">SIMPAN</button>
                  </div>
                  </div>
                </form>
                </div>
                <!-- /.tab-content -->
              <!-- /.card-body -->
            </div>
  </div>
</div>
  <!-- /.content-wrapper -->
<!-- REQUIRED SCRIPTS -->
<script>
  $(document).ready(function () {
    var counter = 0;

    $("#adduraian").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        /*cols += '<td>' + counter + '</td>';*/
        cols += '<td><input type="text" class="form-control" placeholder="Uraian barang" name="uraian[]' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});
</script>
<script>
  $(document).ready(function () {
    var counter = 0;

    $("#addasal").on("click", function () {
        var baris = $("<tr>");
        var kol = "";

        /*cols += '<td>' + counter + '</td>';*/
        kol += '<td><input type="text" class="form-control" placeholder="Asal bahan baku" name="asal_bahan' + counter + '"/></td>';

        kol += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" value="Delete"></td>';
        baris.append(kol);
        $("table.add-column").append(baris);
        counter++;
    });



    $("table.add-column").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});
</script>
<script>
  function date() {
    var d = new Date();
    document.getElementById("date").innerHTML = d;
    document.getElementById("date").addEventListener("click", date);
  }
</script> 
<script>
$(document).ready(function(){
    $('.npwp').mask('00.000.000.0-000.000', {reverse: true});//disni ga jalan mas, cek class nya
    $('.itdp').mask('000000000000', {reverse: true});//disni ga jalan mas, cek class nya
});
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdqycKiRv4dVn6a0a2IdeooaoD7bJSwU8&callback=initMap"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
 </script>
<script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      
      ga('create', 'UA-42863888-9', 'auto');
      ga('send', 'pageview');

      var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 15
                });
                var infoWindow = new google.maps.InfoWindow();

                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude

                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false,
                            title: "Your location"
                        });

//                        infoWindow.setPosition(pos);
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                    }, function () {

                        var pos = {
                            lat: -7.250445,
                            lng: 112.768845
                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false,
                            title: "Your location"
                        });
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                        
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    // handleLocationError(false, infoWindow, map.getCenter());
                   
                    var pos = {

                            lat: -7.250445,
                            lng: 112.768845
                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false,
                            title: "Your location"
                        });
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                        
                    }
                }
            

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                        'Cant get your Location.' :
                        'Error: Your browser doesn\'t support geolocation.');
            }

            function handleEvent(event) {
                document.getElementById('latclicked').value = event.latLng.lat();
                document.getElementById('longclicked').value = event.latLng.lng();
            }

  $("#get-perusahaan").change(function(){

    var id = $(this).val();
    $.ajax({
      type : "POST",
      url : "<?= base_url();?>export/get_perusahaan_id/"+id,
      data : "nama_perusahaan=" + nama_perusahaan,
      dataType : "JSON",
      success : function(res){
          $("#id_perusahaan").val(res.id);
          $("#nama_perusahaan").val(res.nama_perusahaan);
      }
    })

  });
</script>
