<div class="content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<h3>TES</h3>
				<div class="form-group">
					<label>Provinsi</label>
					<select class="form-control" id="provinsi">
						<option value="0">--Pilih--</option>
						<?php 
						foreach($provinsi as $prov){
							echo '<option value="'.$prov->id.'">'.$prov->name.'</option>';
						}
						?>
					</select>
				</div>
				<div class='form-group'>
					<label>Kabupaten/kota</label>
					<select class='form-control' id='kabupaten-kota'>
						<option value='0'>--pilih--</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$.ajaxSetup({
			type:"POST",
			url:"<?php echo base_url('select/ambil_data')?>",
			cache:false,
		});
		$("#provinsi").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kabupaten',id:value},
					success:function(respond){
						$("#kabupaten-kota").html(respond);
					}
				})
			}
		});
	})
</script>