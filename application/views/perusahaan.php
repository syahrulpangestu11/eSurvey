<div class="content-wrapper">

  <div class="card">
    <div class="card-header d-flex p-0">
      <ul class="nav nav-pills ml-auto p-2" style="margin-left:5%!important;margin-right: 5%;">
        <li class="active"><a class="nav-link active" data-toggle="pill" href="#tab_1"> Step 1 </a></li>
        <li><a class="nav-link" data-toggle="pill" href="#tab_2" > Step 2 </a></li>
      </ul>
    </div><!-- /.card-header -->
    <div class="card-body">
      <form action="<?=base_url('Perusahaan/add')?>" method="POST" enctype="multipart/form-data">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <h3 style="text-align: center;">Tentukan Lokasi Anda</h3>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div id="map" style="min-height:400px;"></div>
              <div style="display: none;">Lattitude: <span id="latspan"></span>   Longitude: <span id="lngspan"></span></div>
            </div>
            <div style="display: none"><input class="input100" type="text" name="latitude" id="latclicked" readonly>
              <input class="input100" type="text" name="longitude" id="longclicked" readonly>
            </div>
            <a href="#tab_2" class="nav-link btn btn-success float-right" style="margin: 20px;">Next</a>
          </div>
          <div class="tab-pane" id="tab_2">
            <h3>PROFIL PERUSAHAAN</h3>
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Perusahaan :</label>
              <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="Masukan Nama Perusahaan">
            </div>
            <!-- <input type="hidden" name="nama_perusahaan" id="nama_perusahaan"> -->
            <div class="form-group">
              <label>Date:</label>

              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="waktu">
              </div>
              <!-- /.input group -->
            </div>
          <h5>Alamat Perusahaan</h5>        
          <div class="form-group">
            <label>Provinsi</label>
            <select class="form-control" id="provinsi" name="provinsi">
              <option value="0">--Pilih--</option>
              <?php 
              foreach($provinsi as $prov){
                echo '<option value="'.$prov->id.'">'.$prov->name.'</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Kabupaten</label>
            <select class="form-control" id="kabupaten-kota" name="kabupaten">
              <option>--Pilih--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Kacamatan</label>
            <select class="form-control" id="kecamatan" name="kecamatan">
              <option>--Pilih--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Kelurahan/Desa</label>
            <select class="form-control" id="kelurahan" name="desa">
              <option>--Pilih--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Jalan</label>
            <input type="text" class="form-control" name="jalan" id="jalan" placeholder="Jalan">
          </div>
          <h5>Alamat Pabrik/Gudang</h5>
          <div class="form-group">
            <label>Provinsi</label>
            <select class="form-control" id="provinsi" name="provinsi_gudang">
              <option value="0">--Pilih--</option>
              <?php 
              foreach($provinsi as $prov){
                echo '<option value="'.$prov->id.'">'.$prov->name.'</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Kabupaten</label>
            <select class="form-control" id="kabupaten-kota" name="kabupaten_gudang">
              <option value="0">--Pilih--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Kacamatan</label>
            <select class="form-control" id="kecamatan" name="kecamatan_gudang">
              <option value="0">--Pilih--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Kelurahan/Desa</label>
            <select class="form-control" id="kelurahan" name="desa_gudang">
              <option value="0">--Pilih--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Jalan</label>
            <input type="text" class="form-control" name="jalan_gudang" id="jalan" placeholder="Jalan">
          </div>                                                  

          <!-- /.tab-pane -->
          <label for="exampleInputEmail1">Penanggung Jawab Perusahaan</label>
          <table id="example" class="display" style="width:100%">
            <tbody>
              <tr>
                <td>Penanggung Jawab</td>
                <td><input type="text" class="form-control" name="pj_perusahaan" id="pj_perusahaan" name="pj_perusahaan"></td>
              </tr>
              <tr>
                <td>email</td>
                <td><input type="email" class="form-control" name="e_mail_pj" id="e_mail_pj" name="e_mail_pj"></td>
              </tr>
              <tr>
                <td>No Telp</td>
                <td><input type="number" class="form-control" name="no_telp" id="no_telp" name="no_telp"></td>
              </tr>
            </tbody>
          </table>
          <br>
          <button type="submit" class="btn btn-success" name="submit">SIMPAN</button>
        </div>
      </div>
    </div>
    <!-- /.tab-content -->
    <!-- /.card-body -->
  </div>
</div>
</div>
<!-- /.content-wrapper -->
<!-- REQUIRED SCRIPTS -->
<script type="text/javascript">
  $(function(){
    $.ajaxSetup({
      type:"POST",
      url:"<?php echo base_url('perusahaan/ambil_data')?>",
      cache:false,
    });
    $("#provinsi").change(function(){
      var value=$(this).val();
      if(value>0){
        $.ajax({
          data:{modul:'kabupaten',id:value},
          success:function(respond){
            $("#kabupaten-kota").html(respond);
          }
        })
      }
    });
    $("#kabupaten-kota").change(function(){
      var value=$(this).val();
      if(value>0){
        $.ajax({
          data:{modul:'kecamatan',id:value},
          success:function(respond){
            $("#kecamatan").html(respond);
          }
        })
      } 
    })
    $("#kecamatan").change(function(){
      var value=$(this).val();
      if(value>0){
        $.ajax({
          data:{modul:'kelurahan',id:value},
          success:function(respond){
            $("#kelurahan").html(respond);
          }
        })
      } 
    })
  })
</script>
<script>
  $(document).ready(function () {
    var counter = 0;

    $("#adduraian").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        /*cols += '<td>' + counter + '</td>';*/
        cols += '<td><input type="text" class="form-control" placeholder="Uraian barang" name="uraian[]' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});
</script>
<script>
  $(document).ready(function () {
    var counter = 0;

    $("#addasal").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        /*cols += '<td>' + counter + '</td>';*/
        cols += '<td><input type="text" class="form-control" name="asal_bahan[]" id="asal_bahan" placeholder="asal bahan baku' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});
</script>
<script>
  function date() {
    var d = new Date();
    document.getElementById("date").innerHTML = d;
    document.getElementById("date").addEventListener("click", date);
  }
</script> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdqycKiRv4dVn6a0a2IdeooaoD7bJSwU8&callback=initMap">
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
</script>
<script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      
      ga('create', 'UA-42863888-9', 'auto');
      ga('send', 'pageview');

      var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 15
                });
                var infoWindow = new google.maps.InfoWindow();

                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude

                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: true,
                            title: "Your location"
                        });

                        // infoWindow.setPosition(pos);                        
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                    }, function () {

                        var pos = {
                            lat: -7.250445,
                            lng: 112.768845
                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: true,
                            title: "Your location"
                        });
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                        
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    // handleLocationError(false, infoWindow, map.getCenter());
                   
                    var pos = {

                            lat: -7.250445,
                            lng: 112.768845
                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: true,
                            title: "Your location"
                        });
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                        
                    }
                }
            

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                        'Cant get your Location.' :
                        'Error: Your browser doesn\'t support geolocation.');
            }

            function handleEvent(event) {
                document.getElementById('latclicked').value = event.latLng.lat();
                document.getElementById('longclicked').value = event.latLng.lng();
            }

  $("#get-perusahaan").change(function(){

    var id = $(this).val();
    $.ajax({
      type : "POST",
      url : "<?= base_url();?>export/get_perusahaan_id/"+id,
      data : "nama_perusahaan=" + nama_perusahaan,
      dataType : "JSON",
      success : function(res){
          $("#id_perusahaan").val(res.id);
          $("#nama_perusahaan").val(res.nama_perusahaan);
      }
    })

  });
</script>
<script>
  $(function(){
    $('#datepicker').datepicker({
      autoclose: true
    })
  });
</script>