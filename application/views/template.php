<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>eRPL</title>
  <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="Resources/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="<?php echo base_url(); ?>assets/chosen/docsupport/prism.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
  <!--datepicker-->

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand border-bottom navbar-light bg-gray-white">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->  
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-danger">
    <!-- Brand Logo -->
    <a href="#" class="brand-link bg-gray-light">
        <img src="<?=base_url()?>Resources/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"> E-Survey</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Home
              </p>
            </a>           
          </li>
          <li class="nav-item has-treeview">
            <a href="" class="nav-link">
              <i class="nav-icon fa fa-chalkboard-teacher"></i>
              <p>
                List
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url().'main/import'; ?>" class="nav-link">
                  <i class="fa fa-book-open nav-icon"></i>
                  <p>Import</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url().'main/export'; ?>" class="nav-link">
                  <i class="fa fa-table nav-icon"></i>
                  <p>Export</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url().'main/login'; ?>" class="nav-link">
              <i class="nav-icon fa fa-sign-out-alt"></i>
              <p>
                Sign Out
              </p>
            </a>           
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-block-down">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="https://adminlte.io"></a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?=base_url()?>adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url()?>adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="<?=base_url()?>Resources/adminlte/dist/js/adminlte.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdqycKiRv4dVn6a0a2IdeooaoD7bJSwU8&callback=initMap"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<!--datepicker-->
<script type="text/javascript" src="<?=base_url('adminlte/plugins/moment-develop/min/moment-min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('adminlte/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')?>"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
 </script>
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      
      ga('create', 'UA-42863888-9', 'auto');
      ga('send', 'pageview');

      var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 15
                });
                var infoWindow = new google.maps.InfoWindow();

                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude

                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false,
                            title: "Your location"
                        });

//                        infoWindow.setPosition(pos);
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                    }, function () {

                        var pos = {
                            lat: -7.250445,
                            lng: 112.768845
                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false,
                            title: "Your location"
                        });
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                        
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    // handleLocationError(false, infoWindow, map.getCenter());
                   
                    var pos = {

                            lat: -7.250445,
                            lng: 112.768845
                        };

                        $('#latclicked').val(pos.lat);
                        $('#longclicked').val(pos.lng);

                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false,
                            title: "Your location"
                        });
                        infoWindow.setContent('Your Location.');

                        marker.addListener('click', function () {
                            infoWindow.open(map, marker);
                        });
                        marker.addListener('drag', handleEvent);
                        marker.addListener('dragend', handleEvent);
                        map.setCenter(pos);
                        
                    }
                }
            

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                        'Cant get your Location.' :
                        'Error: Your browser doesn\'t support geolocation.');
            }

            function handleEvent(event) {
                document.getElementById('latclicked').value = event.latLng.lat();
                document.getElementById('longclicked').value = event.latLng.lng();
            }
</script>
<!-- OPTIONAL SCRIPTS -->
<!-- <script src="Resources/adminlte/plugins/chart.js/Chart.min.js"></script>
<script src="Resources/adminlte/dist/js/demo.js"></script>
<script src="Resources/adminlte/dist/js/pages/dashboard3.js"></script> -->
</body>
</html>
