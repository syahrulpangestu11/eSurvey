<!-- <html>
<head>
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>adminlte/dist/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?=base_url();?>adminlte/dist/js/jquery.mask.min.js"></script>
<script src="wget https://github.com/igorescobar/jQuery-Mask-Plugin/blob/master/jquery.mask.min.js "></script>
</head> -->
<div class="container">
    <h3>Problems with jQuery Mask Plugin?</h3>
    <p>Exemplify your problem here :-)</p>
    <hr />
    
    <form role="form">
        
        <div class="form-group">
            <label for="date-field">Date</label>
            <input type="text" class="form-control" id="date-field">
        </div>
    
        <div class="form-group">
            <label for="ip-field">IP</label>
            <input type="text" class="form-control" id="ip-field">
        </div>
        
        <button type="submit" class="btn btn-default">Submit</button>
    
    </div>
    </form>
</div>
</html>
<script>
$(document).ready(function(){
        $('#ip-field').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
            'Z': {
            pattern: /[0-9]/, optional: true
            }
        },
    placeholder: "___.___.___.___"
    });
});
</script>