
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.js"></script>
  <script src="<?=base_url('adminlte/dist/js/bootstrap-datetimepicker.min.js')?>"></script>
  <link rel="stylesheet" type="text/css" href="<?=base_url('adminlte/dist/css/bootstrap-datetimepicker.min.css')?>">
<?php
  $perusahaan = $this->db->get('perusahaan')->result();
?>
<div class="content-wrapper">
<!-- <form style="padding: 10px;">
      <div class="input-group">
        <input type="text" class="form-control" placeslder="Search">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit">
          <i class="fas fa-search"></i>
          </button>
        </div>  
      </div>
    </form> -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div id="map" style="min-height:400px;"></div>
          <div style="display: none;">Lattitude: <span id="latspan"></span>   Longitude: <span id="lngspan"></span></div>
    </div>
    <div style="display: none"><input class="input100" type="text" name="latitude" id="latclicked" readonly>
        <input class="input100" type="text" name="longitude" id="longclicked" readonly>
    </div>
    <br>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          
            <button id="btnAction" class="btn btn-success" onclick="getlokasi()">Lokasi Saya</button>
          
        </div>
        <div class="col-md-4">
          
            <a href="<?=base_url('main/perusahaan')?>" id="btnAction" class="btn btn-success">Form export</a>
          
        </div>
        <div class="col-md-4  ">
          
            <button id="btnAction" class="btn btn-success">Form import</button>
          
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
      <div class="card">
        <div class="list-group text-center">
          <div class="card-header bg-light" id="id_data">
            <h4>Daftar Lokasi Export</h4>
            <!--<select class="form-control" name="loka" id="loka" data-ajax="false">
            <option>Pilih Nama Lokasi</option>
            <?php foreach ($perusahaan as $export) {
              echo '<option value="'.$export->latitude.','.$export->longitude.','.$export->nama_perusahaan.','.$export->jalan.','.$export->desa.','.$export->kecamatan.','.$export->kabupaten.','.$export->provinsi.'">'.$export->nama_perusahaan.'</option>';
            }
            ?>
            </select> -->
            <select class="js-example-basic-multiple form-control" name="loka" id="loka" multiple="multiple">
              <?php foreach ($perusahaan as $export){
                echo '<option value="'.$export->latitude.','.$export->longitude.','.$export->nama_perusahaan.','.$export->jalan.','.$export->desa.','.$export->kecamatan.','.$export->kabupaten.','.$export->provinsi.'">'.$export->nama_perusahaan.'</option>';
              }
              ?>
            </select>
          </div>
          <!-- <?php foreach ($perusahaan as $export):?>
          <a class="btn btn-default" id="loka" onclick="lokasi()" data-ajax="false"><?=$export->nama_perusahaan?></a>
          <?php endforeach ?> -->
        </div>
        
      </div>
      <div class="col-lg-12">
        <button type="button" class="btn btn-default btn-lg" onclick="lokasi()" data-ajax="false">
          <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
          Tampilkan Posisi
        </button>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="list-group text-center">
          <div class="card-header bg-light">
            <h4>Daftar Lokasi import</h4>
          </div>
          <?php foreach ($i as $import):?>
          <a class="btn btn-default" data-ajax="false" onclick="lokasi()"><?=$import->nama_perusahaan?></a>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdqycKiRv4dVn6a0a2IdeooaoD7bJSwU8&callback=initMap">
</script>
<script>
      var map, infoWindow;
            function initMap() {
                /*map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 15
                });
                var infoWindow = new google.maps.InfoWindow();*/
                var myLatlng = new google.maps.LatLng(-2.207251, 113.916669);
                var mapOptions = {
                 zoom: 4,
                 center: myLatlng,
               };
               var infoWindow = new google.maps.InfoWindow;
               var map = new google.maps.Map(document.getElementById('map'), mapOptions);
               google.maps.event.addDomListener(window, 'load', initMap);

            }
            function getlokasi(){
              document.getElementById("btnAction");
              document.getElementById("btnAction");
              if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(function(position){
                  var pos = {
                    lat : position.coords.latitude,
                    lng : position.coords.longitude
                  };
                  var myLatlng = new google.maps.LatLng(pos);
                  var mapOptions = {
                    zoom: 15,
                    center: myLatlng
                  }
                  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                  /*infoWindow.setPosition(pos);
                  infoWindow.setContent('Lokasi anda.');
                  infoWindow.open(map);
                  map.setCenter(pos);*/
                  var marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                  });

                },function() {
                  handleLocationError(true, infoWindow, map.getCenter());
                });
              } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
              }
            }
            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
              infoWindow.setPosition(pos);
              infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
              infoWindow.open(map);  
            }
            function lokasi(){
              var lokasi = document.getElementById('loka').value;
              var koordinat = lokasi.split(',');
                var x = koordinat[0];
                var y = koordinat[1];
                var nama_perusahaan = koordinat[2];
                var jalan = koordinat[3];
                var desa = koordinat[4];
                var kecamatan = koordinat[5];
                var kabupaten = koordinat[6];
                var provinsi = koordinat[7];

              if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(function(position){
                  var pos = {
                    lat : position.coords.latitude,
                    lng : position.coords.longitude
                  };

                },function() {
                  handleLocationError(true, infoWindow, map.getCenter());
                });
              } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
              }
              var myLatlng = new google.maps.LatLng(x,y);
              var mapOptions = {
                zoom: 15,
                scrollwheel: false,
                center: myLatlng
              };
              var map = new google.maps.Map(document.getElementById('map'), mapOptions);
              var contentString = '<div id="content" style="color: black;" data-ajax="false">'+'<div id="" data-ajax="false">'+'<h2>' + nama_perusahaan+'</h2>'+
                '</div>'+
                '<div data-ajax="false">'+
                '<p>'+jalan+'<br>'+desa+'<br>'+kecamatan+'<br>'+kabupaten+'<br>'+provinsi+'</p>'+
                '<form action="https://www.google.com/maps/dir/Current+Location/'+x+y+'">'+
                '<input class="btn btn-success" type="submit" value="Direction"/>'+
                '</form>'+
                '</div>'+
                '</div>';

              var infoWindow = new google.maps.InfoWindow({
                content: contentString
              });

              var marker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: myLatlng,
              });
              google.maps.event.addListener(marker, 'click', function(){
                infoWindow.open(map, marker);
                smoothZoom(map, 15, map.getZoom());
              });
              google.maps.event.addDomListener(window, 'load');

            }
            function togglebounce(){
              if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
              } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
              }
            }
            function smoothZoom(map, max, cnt){
              if(cnt >= max){
                return;
              }
              else {
                z = google.maps.event.addListener(map, 'zoom_changed', function(event){
                  google.maps.event.removeListener(z);
                  smoothZoom(map, max, cnt+1);
                });
                setTimeout(function(){
                  map.setZoom(cnt)}, 50);
              }
            }
</script>
<script>
  $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>