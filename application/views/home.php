<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>eSurvey</title>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>adminlte/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    -->  <!-- AdminLTE -->
    <script src="<?= base_url(); ?>adminlte/dist/js/adminlte.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url(); ?>adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery -->
    <script src="<?= base_url(); ?>adminlte/plugins/jquery/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>adminlte/dist/js/jquery.mask.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>adminlte/dist/js/jquery.mask.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand border-bottom navbar-light bg-gray-white">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->  
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar elevation-4 sidebar-light-danger">
            <!-- Brand Logo -->
            <a href="<?= base_url() ?>" class="brand-link bg-gray-light">
                <img src="<?= base_url(); ?>adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                     style="opacity: .8">
                <span class="brand-text font-weight-light"> E-Survey</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                    <img src="Resources/adminlte/dist/img/avatar.png" class="img-circle elevation-2" alt="User Image">
                  </div>
                  <div class="info">
                    <a href="#" class="d-block">Admin Telkom</a>
                  </div>
                </div> -->

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>" class="nav-link active">
                                <i class="nav-icon fa fa-th"></i>
                                <p>
                                    Home
                                </p>
                            </a>           
                        </li>
                        <li class="nav-item has-treeview">
                            <a class="nav-link">
                                <i class="nav-icon fa fa-chalkboard-teacher"></i>
                                <p>
                                    Survey
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo base_url() . 'main/import'; ?>" class="nav-link">
                                        <i class="fa fa-book-open nav-icon"></i>
                                        <p>Import</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= base_url('export/index') ?>" class="nav-link">
                                        <i class="fa fa-table nav-icon"></i>
                                        <p>Export</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->

        <!-- Content Header (Page header) -->
        <?php $this->load->view($content) ?>

        <br>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-block-down">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2018 <a href="https://adminlte.io"></a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-23581568-13');
    </script>
    <script>
        $(document).ready(function () {
            var counter = 0;

            $('#addrow').on("click", function () {
                var newRow = $("</tr>");
                var cols = "";

                cols += '<td><input type="text" class="form-control" name="uraian" id="uraian" placeholder="Uraian barang' + counter + '"></td>';
                cols += '<td><input type="button" class="btndel btn btn-danger" value="remove"></td>';
                newRow.append(cols);
                $("table.order-list").append(newRow);
                counter++;
            });
            $("table.order-list").on("click", ".btndel", function (event) {
                $(this).closest("tr").remove();
                counter -= 1
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#ip-field').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                },
                placeholder: "___.___.___.___"
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#navlink').one
        });
    </script>
    <!-- OPTIONAL SCRIPTS -->
    <!-- <script src="Resources/adminlte/plugins/chart.js/Chart.min.js"></script>
    <script src="Resources/adminlte/dist/js/demo.js"></script>
    <script src="Resources/adminlte/dist/js/pages/dashboard3.js"></script> -->
</body>
</html>
