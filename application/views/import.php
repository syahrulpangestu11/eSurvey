<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->
<div class="content-wrapper">
    <div class="card">
        <div class="card-header d-flex p-0">
            <ul class="nav nav-pills ml-auto p-2" style="margin-left:5%!important;margin-right: 5%;">
                <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"> Step 1 </a></li>
                <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"> Step 2 </a></li>
                <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab"> Step 3 </a></li>
                <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab"> Step 4 </a></li>
                <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab"> Step 5 </a></li>
            </ul>
        </div><!-- /.card-header -->


        <div class="card-body">
            <form action="<?= base_url(); ?>c_import/upload" method="post" enctype="multipart/form-data">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <h3 style="text-align: center;">Tentukan Lokasi Anda</h3>
                        <!--           <?php foreach ($import as $i): ?>
                            <?= $i->id_perusahaan ?>
                        <?php endforeach ?> -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div id="map" style="min-height:400px;"></div>
                            <div style="display: none;">Lattitude: <span id="latspan"></span>   Longitude: <span id="lngspan"></span></div>
                        </div>
                        <div style="display: none">
                            <input class="input100" type="text" name="latitude" id="latclicked" readonly>
                            <input class="input100" type="text" name="longitude" id="longclicked" readonly>
                        </div>
                        <a href="#tab_2" class="btn nav-link btn-success float-right" style="margin: 20px;">Next</a>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="form-group">
                            <h5 for="exampleInputEmail1">Identitas Perusahaan :</h5>
                            <div class="form-group" id="exampleInputEmail1">
                                <input type="text" class="form-control" id="exampleInputEmail1" name="nama_perusahaan" placeholder="Nama Perusahaan">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputEmail1" name="alamat_perusahaan" placeholder="Alamat Perusahaan">  
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" id="exampleInputEmail1" name="no_telp" placeholder="Nomor Telepon">  
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email">  
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputEmail1" name="nama_pj" placeholder="Nama Penanggung Jawab">  
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" id="exampleInputEmail1" name="telp_pj" placeholder="Nomor Telepon">  
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <h5 for="exampleInputEmail1">I. Aspek Legalitas</h5>
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                <h6>NPWP</h6>
                                <input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="00.000.000.0-000.000" data-mask-selectonfocus="true" placeholder="Nomor" id="no_npwp" style="margin-bottom: 1%" name="no_npwp">
                                <input type="date" class="form-control" id="tgl_npwp" name="tgl_npwp">
                                </tr>
                                <tr>
                                <h6>TDP</h6><input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="00.00.0.00.00000" data-mask-selectonfocus="true" placeholder="Nomor" id="no_tdp" name="no_tdp" style="margin-bottom: 1%" ><input type="date" class="form-control" id="tgl_tdp" name="tgl_tdp">
                                </tr>
                                <tr>
                                <h6>SIUP</h6><input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="000/0000A/000.00.00.0000" data-mask-selectonfocus="true" placeholder="Nomor" id="no_siup" name="no_siup" style="margin-bottom: 1%"><input type="date" class="form-control" id="tgl_siup" name="tgl_siup">
                                </tr>
                                <tr>
                                <h6>IUI/TDI</h6><input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="000/00.00/AAA.A/AA/0000" data-mask-selectonfocus="true" placeholder="Nomor" id="no_iuitdi" name="no_iuitdi" style="margin-bottom: 1%"><input type="date" class="form-control" id="tgl_iuitdi" name="tgl_iuitdi">
                                </tr>
                                <tr>
                                <h6>API</h6><input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="000000000-A" data-mask-selectonfocus="true" placeholder="Nomor" style="margin-bottom: 1%" id="no_api" name="no_api" >
                                <input type="date" class="form-control" id="tgl_api" name="tgl_api">
                                </tr>
                                <tr>
                                <h6>NIK</h6><input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="00000000000000000" data-mask-selectonfocus="true" placeholder="Nomor" id="no_nik" style="margin-bottom: 1%" name="no_nik">
                                <input type="date" class="form-control" id="tgl_nik" name="tgl_nik">
                                </tr>
                                </tbody>
                            </table>            
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <h5>II. Komoditas Impor</h5>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" style="margin-bottom: 1%">Tambah</button> <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable')" />

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">II. Komoditas Impor</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nama_barang" name="barang_komoditas" placeholder="Nama Barang">  
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="negara_asal" name="negara_asal" placeholder="Asal">  
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="dokumen_perizinan" name="dokumen_perizinan" placeholder="Dokumen Perizinan">  
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" id="btn-add" class="btn btn-primary">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="dataTable" style="text-align: center" class="table table-hover">
                            <thead>
                                <tr>
                                    <td><h6>Pilih</h6></td>
                                    <td><h6>Nama Barang</h6></td>
                                    <td><h6>Negara Asal</h6></td>
                                    <td><h6>Dokumen Izin</h6></td>
                                </tr>
                            </thead>
                            <tbody id="tbody">

                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(function () {
                                $('#btn-add').click(function () {
                                    var barang_komoditas = $('input[name="barang_komoditas"]').val();
                                    var negara_asal = $('input[name="negara_asal"]').val();
                                    var dokumen_perizinan = $('input[name="dokumen_perizinan"]').val();


                                    var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + barang_komoditas + '<input type="hidden" name="list_barang_komoditas[]" value="'+ barang_komoditas +'"></td> <td>' + negara_asal + '<input type="hidden" name="list_negara_asal[]" value="'+ negara_asal +'"></td> <td>' + dokumen_perizinan + '<input type="hidden" name="list_dokumen_perizinan[]" value="'+ dokumen_perizinan +'"></td></tr>';

                                    $('#tbody').append(tr);
                                });
                            });
                        </script>

                        <br>
                        <h5>III. Penggunaan Barang Impor</h5>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal1" style="margin-bottom: 1%">Tambah</button>
                        <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable1')" />
                        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">III. Penggunaan Barang Impor</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type=text"" class="form-control" id="nama_barang" name="barang_penggunaan" placeholder="Nama Barang">  
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" size="1" id="penggunaan" name="penggunaan">
                                                <option placeholder="Penggunaan" selected="selected">
                                                    Penggunaan
                                                </option>
                                                <option value="Bahan Baku">
                                                    Bahan Baku
                                                </option>
                                                <option value="Penolong">
                                                    Penolong
                                                </option>
                                                <option value="Modal">
                                                    Modal
                                                </option>
                                                <option value="Dijual Langsung">
                                                    Dijual Langsung
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" id="btn-add1" class="btn btn-primary">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table style="text-align: center" id="dataTable1" class="table table-hover">
                            <thead>
                                <tr>
                                    <td><h6>Pilih</h6></td>
                                    <td><h6>Nama Barang</h6></td>
                                    <td><h6>Penggunaan</h6></td>
                                </tr>
                            </thead>
                            <tbody id="tbody1">

                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(function () {
                                $('#btn-add1').click(function () {
                                    var barang_penggunaan = $('input[name="barang_penggunaan"]').val();
                                    var penggunaan = $('select[name="penggunaan"]').val();

                                    var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + barang_penggunaan + '<input type="hidden" name="list_barang_penggunaan[]" value="'+ barang_penggunaan +'"></td> <td>' + penggunaan + '<input type="hidden" name="list_penggunaan[]" value="'+ penggunaan +'"></td></tr>';

                                    $('#tbody1').append(tr);
                                });
                            });
                        </script>

                        <br>
                        <h5>IV. Distribusi Barang Impor (Khusus untuk Trading/ Pemegang API-U)</h5>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal2" style="margin-bottom: 1%">Tambah</button>
                        <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable2')" />
                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">IV. Distribusi Barang Impor (Khusus untuk Trading/ Pemegang API-U)</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type=text"" class="form-control" id="nama_pembeli" name="nama_pembeli" placeholder="Nama Pembeli">  
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nama_barang" name="barang_distribusi" placeholder="Nama Barang">  
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" id="btn-add2" class="btn btn-primary">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table style="text-align: center" class="table table-hover" id="dataTable2">
                            <thead>
                                <tr>
                                    <td><h6>Pilih</h6></td>
                                    <td><h6>Nama Pembeli</h6></td>
                                    <td><h6>Nama Barang</h6></td>
                                </tr>
                            </thead>
                            <tbody id="tbody2">

                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(function () {
                                $('#btn-add2').click(function () {
                                    var nama_pembeli = $('input[name="nama_pembeli"]').val();
                                    var barang_distribusi = $('input[name="barang_distribusi"]').val();

                                    var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + nama_pembeli + '<input type="hidden" name="list_nama_pembeli[]" value="'+ nama_pembeli +'"></td> <td>' + barang_distribusi + '<input type="hidden" name="list_barang_distribusi[]" value="'+ barang_distribusi +'"></td></tr>';

                                    $('#tbody2').append(tr);
                                });
                            });
                        </script>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_4">
                        <h5>V. Aspek Baku Produksi</h5>
                        <div class="form-group">
                            <h6>A. Bahan Baku Produksi</h6>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal3" style="margin-bottom: 1%">Tambah</button>
                            <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable3')" />
                            <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">A. Bahan Baku Produksi</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="jenis_bahan" name="jenis_bahan" placeholder="Jenis Bahan Baku">  
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="kebutuhan" name="kebutuhan" placeholder="Kebutuhan Per tahun">  
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="asal_bahan" name="asal_bahan" placeholder="Negara/ Daerah asal">  
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" id="btn-add3" class="btn btn-primary">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table style="text-align: center" class="table table-hover" id="dataTable3">
                                <thead>
                                    <tr>
                                        <td><h6>Pilih</h6></td>
                                        <td><h6>Jenis Bahan</h6></td>
                                        <td><h6>Kebutuhan perTahun</h6></td>
                                        <td><h6>Asal Bahan</h6></td>
                                    </tr>
                                </thead>
                                <tbody id="tbody3">

                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(function () {
                                    $('#btn-add3').click(function () {
                                        var jenis_bahan = $('input[name="jenis_bahan"]').val();
                                        var kebutuhan = $('input[name="kebutuhan"]').val();
                                        var asal_bahan = $('input[name="asal_bahan"]').val();

                                        var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + jenis_bahan + '<input type="hidden" name="list_jenis_bahan[]" value="'+ jenis_bahan +'"></td> <td>' + kebutuhan + '<input type="hidden" name="list_kebutuhan[]" value="'+ kebutuhan +'"></td> <td>' + asal_bahan + '<input type="hidden" name="list_asal_bahan[]" value="'+ asal_bahan +'"></td></tr>';

                                        $('#tbody3').append(tr);
                                    });
                                });
                            </script>

                            <br>
                            <h6>B. Hasil Produksi</h6>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal4" style="margin-bottom: 1%">Tambah</button>
                            <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable4')" />
                            <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">B. Hasil Produksi</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="barang_produksi" name="barang_produksi" placeholder="Nama Barang">  
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="kapasitas" name="kapasitas" placeholder="Kapasitas Per tahun ">  
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" id="btn-add4" class="btn btn-primary">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table style="text-align: center" id="dataTable4" class="table table-hover">
                                <thead>
                                    <tr>
                                        <td><h6>Pilih</h6></td>
                                        <td><h6>Nama Barang</h6></td>
                                        <td><h6>Kapasitas perTahun</h6></td>
                                    </tr>
                                </thead>
                                <tbody id="tbody4">

                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(function () {
                                    $('#btn-add4').click(function () {
                                        var barang_produksi = $('input[name="barang_produksi"]').val();
                                        var kapasitas = $('input[name="kapasitas"]').val();

                                        var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + barang_produksi + '<input type="hidden" name="list_barang_produksi[]" value="'+ barang_produksi +'"></td> <td>' + kapasitas + '<input type="hidden" name="list_kapasitas[]" value="'+ kapasitas +'"></td></tr>';

                                        $('#tbody4').append(tr);
                                    });
                                });
                            </script>

                            <br>
                            <h6>C. Pemasaran</h6>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal5" style="margin-bottom: 1%">Tambah</button>
                            <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable5')" />

                            <div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">C. Pemasaran</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="barang_pemasaran" name="barang_pemasaran" placeholder="Nama Barang">  
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="00%" data-mask-reverse="true" data-mask-maxlength="true" id="p_lokal" name="p_lokal" placeholder="Lokal (%)">  
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="00%" data-mask-reverse="true" data-mask-maxlength="true" id="p_ekspor" name="p_ekspor" placeholder="Ekspor (%)">  
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" id="btn-add5" class="btn btn-primary">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table style="text-align: center" class="table table-hover" id="dataTable5">
                                <thead>
                                    <tr>
                                        <td><h6>Pilih</h6></td>
                                        <td><h6>Nama Barang</h6></td>
                                        <td><h6>Lokal %</h6></td>
                                        <td><h6>Ekspor %</h6></td>
                                    </tr>
                                </thead>
                                <tbody id="tbody5">

                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(function () {
                                    $('#btn-add5').click(function () {
                                        var barang_pemasaran = $('input[name="barang_pemasaran"]').val();
                                        var p_lokal = $('input[name="p_lokal"]').val();
                                        var p_ekspor = $('input[name="p_ekspor"]').val();

                                        var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + barang_pemasaran + '<input type="hidden" name="list_barang_pemasaran[]" value="'+ barang_pemasaran +'"></td> <td>' + p_lokal + '<input type="hidden" name="list_p_lokal[]" value="'+ p_lokal +'"></td> <td>' + p_ekspor + '<input type="hidden" name="list_p_ekspor[]" value="'+ p_ekspor +'"></td></tr>';

                                        $('#tbody5').append(tr);
                                    });
                                });
                            </script>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_5">
                        <h5>VI. Laporan Realisasi Impor</h5>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal6" style="margin-bottom: 1%">Tambah</button>
                        <input type="button" class="btn btn-primary btn-sm" style="margin-bottom: 1%" value="Hapus" onclick="deleteRow('dataTable6')" />

                        <div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">VI. Laporan Realisasi Impor</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="komoditi" name="komoditi" placeholder="Komoditi">                
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="asal_impor" name="asal_impor" placeholder="Asal Import">                
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="#### Kg" data-mask-reverse="true" data-mask-maxlength="false" id="volume" name="volume" placeholder="Volume (Kg)">                
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control simple-field-data-mask-selectonfocus" type="text" data-mask="#.###,## $" data-mask-reverse="true" data-mask-maxlength="false" id="nilai" name="nilai" placeholder="Nilai (USD)">                
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" id="btn-add6" class="btn btn-primary">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table style="text-align: center" class="table table-hover" id="dataTable6">
                            <thead>
                                <tr>
                                    <td><h6>Pilih</h6></td>
                                    <td><h6>Komoditi</h6></td>
                                    <td><h6>Asal Import</h6></td>
                                    <td><h6>Volume</h6></td>
                                    <td><h6>Nilai</h6></td>

                                </tr>
                            </thead>
                            <tbody id="tbody6">
                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(function () {
                                $('#btn-add6').click(function () {
                                    var komoditi = $('input[name="komoditi"]').val();
                                    var asal_impor = $('input[name="asal_impor"]').val();
                                    var volume = $('input[name="volume"]').val();
                                    var nilai = $('input[name="nilai"]').val();

                                    var tr = '<tr><td><input type="checkbox" name="chk"/></td> <td>' + komoditi + '<input type="hidden" name="list_komoditi[]" value="'+ komoditi +'"></td> <td>' + asal_impor + '<input type="hidden" name="list_asal_impor[]" value="'+ asal_impor +'"></td> <td>' + volume + '<input type="hidden" name="list_volume[]" value="'+ volume +'"></td> <td>' + nilai + '<input type="hidden" name="list_nilai[]" value="'+ nilai +'"></td></tr>';

                                    $('#tbody6').append(tr);
                                });
                            });
                        </script>

                        <br>
                        <h5>VII. Informasi Tambahan</h5>
                        <div class="form-group">
                            <h6>Hadir dalam pertemuan :</h6>
                            <textarea type="text" class="form-control" id="hadir" name="hadir"></textarea>                
                        </div>
                        <div class="form-group">
                            <h6>Permasalahan :</h6>
                            <textarea type="text" class="form-control" id="permasalahan" name="permasalahan"></textarea>
                        </div>
                        <div class="form-group">
                            <h6>Harapan/ Solusi :</h6>
                            <textarea type="text" class="form-control" id="harapan" name="harapan"></textarea>
                        </div>
                        <h6>Contoh barang impor/ hasil produksi :</h6>
                        <div class="form-group">
                            <textarea type="text" class="form-control" id="contoh_barang" name="contoh_barang"></textarea>
                        </div>            
                        <input type="submit" name="kirim" id="save" class="btn btn-success" value="Kirim"></input>

                    </div>
                </div>
            </form>
            <!-- /.tab-content -->
        </div><!-- /.card-body -->
    </div>

</div>
<script type="text/javascript">
    function deleteRow(tableID) {
        try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;

            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if (null != chkbox && true == chkbox.checked) {
                    if (rowCount <= 1) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }


            }
        } catch (e) {
            alert(e);
        }
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdqycKiRv4dVn6a0a2IdeooaoD7bJSwU8&callback=initMap"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-42863888-9', 'auto');
    ga('send', 'pageview');
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 15
        });
        var infoWindow = new google.maps.InfoWindow();

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude

                };
                $('#latclicked').val(pos.lat);
                $('#longclicked').val(pos.lng);
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    draggable: false,
                    title: "Your location"
                });

                //                        infoWindow.setPosition(pos);
                infoWindow.setContent('Your Location.');
                marker.addListener('click', function () {
                    infoWindow.open(map, marker);
                });
                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
                map.setCenter(pos);
            }, function () {
                var pos = {
                    lat: -7.250445,
                    lng: 112.768845
                };
                $('#latclicked').val(pos.lat);
                $('#longclicked').val(pos.lng);
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    draggable: false,
                    title: "Your location"
                });
                infoWindow.setContent('Your Location.');
                marker.addListener('click', function () {
                    infoWindow.open(map, marker);
                });
                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
                map.setCenter(pos);

                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            // handleLocationError(false, infoWindow, map.getCenter());
            var pos = {
                lat: -7.250445,
                lng: 112.768845
            };

            $('#latclicked').val(pos.lat);
            $('#longclicked').val(pos.lng);
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable: false,
                title: "Your location"
            });
            infoWindow.setContent('Your Location.');
            marker.addListener('click', function () {
                infoWindow.open(map, marker);
            });
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            map.setCenter(pos);
        }
    }
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                'Cant get your Location.' :
                'Error: Your browser doesn\'t support geolocation.');
    }

    function handleEvent(event) {
        document.getElementById('latclicked').value = event.latLng.lat();
        document.getElementById('longclicked').value = event.latLng.lng();
    }
</script>
<script type="text/javascript" src="<?= base_url(); ?>adminlte/plugins/maskedinput/jquery.mask.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>adminlte/plugins/maskedinput/jquery.mask.test.js"></script>













