-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2018 at 04:48 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esurvey`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan_produksi`
--

CREATE TABLE `bahan_produksi` (
  `id_bahan_produksi` int(11) NOT NULL,
  `jenis_bahan` varchar(50) NOT NULL,
  `kebutuhan` varchar(50) NOT NULL,
  `asal_bahan` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `distribusi_barang`
--

CREATE TABLE `distribusi_barang` (
  `id_distribusi_barang` int(11) NOT NULL,
  `nama_pembeli` varchar(50) NOT NULL,
  `barang_distribusi` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `export`
--

CREATE TABLE `export` (
  `id` int(11) NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `sbh` varchar(50) NOT NULL,
  `itdp` varchar(50) NOT NULL,
  `tgl_itdp` varchar(50) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `siup` varchar(50) NOT NULL,
  `tgl_siup` varchar(100) NOT NULL,
  `jenis_eksportir` varchar(50) NOT NULL,
  `iui` varchar(50) NOT NULL,
  `tgl_iui` varchar(50) NOT NULL,
  `bidang_usaha` varchar(50) NOT NULL,
  `uraian` varchar(50) NOT NULL,
  `asal_bahan` varchar(50) NOT NULL,
  `ngr_ekspor` varchar(50) NOT NULL,
  `kap_produksi` varchar(50) NOT NULL,
  `kr_asal_barang` varchar(50) NOT NULL,
  `hadir` varchar(20) NOT NULL,
  `temuan` varchar(50) NOT NULL,
  `arahan` varchar(50) NOT NULL,
  `saran` varchar(100) NOT NULL,
  `sample` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_produksi`
--

CREATE TABLE `hasil_produksi` (
  `id_hasil_produksi` int(11) NOT NULL,
  `barang_produksi` varchar(50) NOT NULL,
  `kapasitas` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `import`
--

CREATE TABLE `import` (
  `id_perusahaan` int(20) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `no_telp` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_pj` varchar(50) NOT NULL,
  `telp_pj` varchar(50) NOT NULL,
  `no_npwp` int(50) NOT NULL,
  `no_tdp` int(50) NOT NULL,
  `no_siup` int(50) NOT NULL,
  `no_iuitdi` int(50) NOT NULL,
  `no_api` int(50) NOT NULL,
  `no_nik` int(50) NOT NULL,
  `tgl_npwp` date NOT NULL,
  `tgl_tdp` date NOT NULL,
  `tgl_siup` date NOT NULL,
  `tgl_iuitdi` date NOT NULL,
  `tgl_api` date NOT NULL,
  `tgl_nik` date NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `hadir` varchar(1000) NOT NULL,
  `permasalahan` varchar(1000) NOT NULL,
  `harapan` varchar(1000) NOT NULL,
  `contoh_barang` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komoditas_impor`
--

CREATE TABLE `komoditas_impor` (
  `id_komoditas_impor` int(11) NOT NULL,
  `barang_komoditas` varchar(50) NOT NULL,
  `negara_asal` varchar(50) NOT NULL,
  `dokumen_perizinan` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemasaran`
--

CREATE TABLE `pemasaran` (
  `id_pemasaran` int(11) NOT NULL,
  `barang_pemasaran` varchar(50) NOT NULL,
  `p_lokal` varchar(50) NOT NULL,
  `p_ekspor` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penggunaan_barang`
--

CREATE TABLE `penggunaan_barang` (
  `id_barang_penggunaan` int(11) NOT NULL,
  `barang_penggunaan` varchar(50) NOT NULL,
  `penggunaan` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `jalan` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `jalan_gudang` varchar(50) NOT NULL,
  `desa_gudang` varchar(50) NOT NULL,
  `kecamatan_gudang` varchar(50) NOT NULL,
  `kabupaten_gudang` varchar(50) NOT NULL,
  `provinsi_gudang` varchar(50) NOT NULL,
  `pj_perusahaan` varchar(30) NOT NULL,
  `e_mail_pj` varchar(100) NOT NULL,
  `no_telp` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `realisasi_impor`
--

CREATE TABLE `realisasi_impor` (
  `id_realisasi_impor` int(11) NOT NULL,
  `komoditi` varchar(50) NOT NULL,
  `asal_impor` varchar(50) NOT NULL,
  `volume` varchar(50) NOT NULL,
  `nilai` varchar(50) NOT NULL,
  `id_perusahaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan_produksi`
--
ALTER TABLE `bahan_produksi`
  ADD PRIMARY KEY (`id_bahan_produksi`),
  ADD KEY `id_perusahaan` (`id_perusahaan`);

--
-- Indexes for table `distribusi_barang`
--
ALTER TABLE `distribusi_barang`
  ADD PRIMARY KEY (`id_distribusi_barang`),
  ADD KEY `id_import` (`id_perusahaan`);

--
-- Indexes for table `export`
--
ALTER TABLE `export`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_produksi`
--
ALTER TABLE `hasil_produksi`
  ADD PRIMARY KEY (`id_hasil_produksi`),
  ADD KEY `id_import` (`id_perusahaan`);

--
-- Indexes for table `import`
--
ALTER TABLE `import`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `komoditas_impor`
--
ALTER TABLE `komoditas_impor`
  ADD PRIMARY KEY (`id_komoditas_impor`),
  ADD KEY `id_import` (`id_perusahaan`);

--
-- Indexes for table `pemasaran`
--
ALTER TABLE `pemasaran`
  ADD PRIMARY KEY (`id_pemasaran`),
  ADD KEY `id_import` (`id_perusahaan`);

--
-- Indexes for table `penggunaan_barang`
--
ALTER TABLE `penggunaan_barang`
  ADD PRIMARY KEY (`id_barang_penggunaan`),
  ADD KEY `id_import` (`id_perusahaan`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `realisasi_impor`
--
ALTER TABLE `realisasi_impor`
  ADD PRIMARY KEY (`id_realisasi_impor`),
  ADD KEY `id_import` (`id_perusahaan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan_produksi`
--
ALTER TABLE `bahan_produksi`
  MODIFY `id_bahan_produksi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `distribusi_barang`
--
ALTER TABLE `distribusi_barang`
  MODIFY `id_distribusi_barang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `export`
--
ALTER TABLE `export`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hasil_produksi`
--
ALTER TABLE `hasil_produksi`
  MODIFY `id_hasil_produksi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `import`
--
ALTER TABLE `import`
  MODIFY `id_perusahaan` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `komoditas_impor`
--
ALTER TABLE `komoditas_impor`
  MODIFY `id_komoditas_impor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemasaran`
--
ALTER TABLE `pemasaran`
  MODIFY `id_pemasaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penggunaan_barang`
--
ALTER TABLE `penggunaan_barang`
  MODIFY `id_barang_penggunaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `realisasi_impor`
--
ALTER TABLE `realisasi_impor`
  MODIFY `id_realisasi_impor` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bahan_produksi`
--
ALTER TABLE `bahan_produksi`
  ADD CONSTRAINT `bahan_produksi_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

--
-- Constraints for table `distribusi_barang`
--
ALTER TABLE `distribusi_barang`
  ADD CONSTRAINT `distribusi_barang_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

--
-- Constraints for table `hasil_produksi`
--
ALTER TABLE `hasil_produksi`
  ADD CONSTRAINT `hasil_produksi_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

--
-- Constraints for table `komoditas_impor`
--
ALTER TABLE `komoditas_impor`
  ADD CONSTRAINT `komoditas_impor_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

--
-- Constraints for table `pemasaran`
--
ALTER TABLE `pemasaran`
  ADD CONSTRAINT `pemasaran_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

--
-- Constraints for table `penggunaan_barang`
--
ALTER TABLE `penggunaan_barang`
  ADD CONSTRAINT `penggunaan_barang_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

--
-- Constraints for table `realisasi_impor`
--
ALTER TABLE `realisasi_impor`
  ADD CONSTRAINT `realisasi_impor_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `import` (`id_perusahaan`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
